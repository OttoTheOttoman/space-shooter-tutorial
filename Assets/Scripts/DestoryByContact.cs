﻿using UnityEngine;
using System.Collections;

public class DestoryByContact : MonoBehaviour
{
    public GameObject asteroidExplosion, playerExplosion;
    private GameController gameController;
    public int scoreValue;

    void Start ()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("GameController not found in DestroyByContact");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Boundary"))
            return;

        if (other.CompareTag("Player"))
        {
            Instantiate(playerExplosion, transform.position, transform.rotation);
        }
        else
        {
            Instantiate(asteroidExplosion, transform.position, transform.rotation);
        }

        gameController.addScore(scoreValue);
        Destroy(other.gameObject);
        Destroy(gameObject);
        
    }
}
